from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.button import Button


class MyApp(App):
    Liste_btn = ["7", "8", "9", "*",
                 "4", "5", "6", "/",
                 "1", "2", "3", "-",
                 "C", "0", ".", "+",
                 ]

    def build(self):
        self.box1 = GridLayout(cols=1)
        self.box1.rows = 3

        self.grid_haut = GridLayout(cols=1)
        self.grid_clavier = GridLayout(cols=4)
        self.grid_egal= GridLayout(cols = 2)

        #Creation du Grid d'affichage de résultat
        self.grid_resultat = BoxLayout(orientation="horizontal")


        self.label_dur_resultat=Label(text="Votre Resultat")
        self.grid_resultat.add_widget(self.label_dur_resultat)

        #ajout du Grid_Resultat au Grid_Haut
        self.grid_haut.add_widget(self.grid_resultat)


        #Creation et placement du Label Resultat
        self.label_resultat = Label(text="Resultat", font_size=30, )
        self.grid_resultat.add_widget(self.label_resultat)

        #Creation et placement du label Saisie
        self.label_saisie = Label()
        self.grid_haut.add_widget(self.label_saisie)

        #dimensions dans le grid Haut
        self.grid_haut.size_hint_y=0.8

        #dimensions dans le grid Clavier
        self.grid_clavier.padding = 10
        self.grid_clavier.spacing = 10

        #dimensions dans le grid Egal
        self.grid_egal.size_hint_y = 0.3
        self.grid_egal.padding = 10
        self.grid_egal.spacing = 10



        self.liste_pour_calcul=[]
        self.boutons = []
        self.bouton_egal()
        self.grid_egal.size_hint_y = 0.3



        self.box1.add_widget(self.grid_haut)
        self.box1.add_widget(self.grid_clavier)
        self.box1.add_widget(self.grid_egal)




        for i in self.Liste_btn:
            self.ajouterBouton(i)

        return self.box1

    def ajouterBouton(self, texte):
        bouton = Button(text=texte, font_size=30, background_color="green")
        bouton.bind(on_press=self.texte_calcul)
        self.grid_clavier.add_widget(bouton)

    def texte_calcul(self, bouton):
        self.liste_pour_calcul.append(bouton.text)
        if bouton.text == "=":
            self.liste_pour_calcul.remove("=")
            resultat = self.calculer_resultat()
            self.label_resultat.text = str(resultat)
        elif bouton.text == "C":
            self.fonction_cancel()
        else:
            self.label_saisie.text += bouton.text

    def calculer_resultat(self):
        expression = "".join(self.liste_pour_calcul) #la fonction join sert à concatener (mettre bout à bout) les caractères de la liste
        #le '' pour qu'il n'y ai pas de separateur entre les éléments
        try:
            print(expression)
            resultat = eval(expression) # Utiliser eval pour évaluer l'expression. "eval" sert à évaluer une
            # expression mathématique qui est représentée sous forme de string
            self.liste_pour_calcul = [str(resultat)]  # Réinitialiser la liste pour la prochaine opération
            return resultat
        except Exception as e:
            self.liste_pour_calcul = []  # Réinitialiser la liste en cas d'erreur
            return f"Erreur: {e}"

    def fonction_cancel(self):
        self.liste_pour_calcul.clear()
        self.label_saisie.text = "0"
        return

    def bouton_egal(self):
        btn_egal = Button(text="=", background_color="lime")
        self.grid_egal.add_widget(btn_egal)
        btn_egal.bind(on_press=self.texte_calcul)

if __name__ == "__main__":
    MyApp().run()
